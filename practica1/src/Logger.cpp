#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#define Buffer 200
int main (void){
	int fd;
	char mensaje[Buffer];

	if(mkfifo("tuberia",0660)<0){
		perror("error al crear tuberia");
		return -1;
	}

	if ((fd=open("tuberia",O_RDONLY))<0){
		perror("no se pudo abrir");
		return -1;
	}

	while (read(fd,mensaje,sizeof (char)*Buffer)>0)
		printf("%s\n",mensaje);
	
	close(fd);
	unlink("tuberia");
	return 0;
}




