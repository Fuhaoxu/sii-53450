
	#include <sys/mman.h>
	#include <sys/types.h>
	#include <sys/stat.h>
	#include "memoria.h"
	#include <unistd.h>
	#include <stdlib.h>
	#include <fcntl.h>

int main(){
	Memoria *dat;
	char *direccion;
	int fd;
	
	if ((fd=open("datos.txt",O_CREAT | O_RDWR)< 0)){
		perror("Error al abrir el fichero de datos desde el bot.");
		return 1;
	}
	dat=(Memoria*)mmap(NULL,sizeof(&dat),PROT_READ|PROT_WRITE,MAP_SHARED,fd,0);
	close (fd);

	while(1){
		usleep(25000);
		if(dat->esfera.centro.y > (dat->raqueta1.y1 + dat->raqueta1.y2)/2 )
			dat->accion = 1;
		else if(dat->esfera.centro.y < (dat->raqueta1.y1 + dat->raqueta1.y2)/2 )
			dat->accion = -1;
		else
			dat->accion = 0;
	};
	return 0;

	/*while(1){
		if(dat->raqueta1.y1 < dat->esfera.centro.y)
		dat->accion=1;	
		if(dat->raqueta1.y1 > dat->esfera.centro.y)
		dat->accion=-1;	
	usleep(25000);
	}
	munmap(direccion,sizeof(*(dat))); 
	return 0;
	*/
}
